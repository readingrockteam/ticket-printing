﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.DirectoryEntry1 = New System.DirectoryServices.DirectoryEntry
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.rbNot = New System.Windows.Forms.RadioButton
        Me.rbApprove = New System.Windows.Forms.RadioButton
        Me.cmdNot = New System.Windows.Forms.Button
        Me.cmdApprove = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.eDraw = New AxEModelView.AxEModelViewControl
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.RadioButton2 = New System.Windows.Forms.RadioButton
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.cmdRelease = New System.Windows.Forms.Button
        Me.cbxPrinter = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtFFMID = New System.Windows.Forms.TextBox
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Packing_SolutionDataSet = New ticket_printing.Packing_SolutionDataSet
        Me.Release_listBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Release_listTableAdapter = New ticket_printing.Packing_SolutionDataSetTableAdapters.release_listTableAdapter
        Me.TableAdapterManager = New ticket_printing.Packing_SolutionDataSetTableAdapters.TableAdapterManager
        Me.SwmastTableAdapter = New ticket_printing.Packing_SolutionDataSetTableAdapters.swmastTableAdapter
        Me.SwmastBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.eDraw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.Packing_SolutionDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Release_listBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SwmastBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.rbNot)
        Me.FlowLayoutPanel1.Controls.Add(Me.rbApprove)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdNot)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdApprove)
        Me.FlowLayoutPanel1.Controls.Add(Me.Button1)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(12, 38)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(618, 34)
        Me.FlowLayoutPanel1.TabIndex = 3
        '
        'rbNot
        '
        Me.rbNot.AutoSize = True
        Me.rbNot.Location = New System.Drawing.Point(3, 3)
        Me.rbNot.Name = "rbNot"
        Me.rbNot.Size = New System.Drawing.Size(91, 17)
        Me.rbNot.TabIndex = 6
        Me.rbNot.Text = "Not Approved"
        Me.rbNot.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.rbNot.UseVisualStyleBackColor = True
        '
        'rbApprove
        '
        Me.rbApprove.AutoSize = True
        Me.rbApprove.Checked = True
        Me.rbApprove.Location = New System.Drawing.Point(100, 3)
        Me.rbApprove.Name = "rbApprove"
        Me.rbApprove.Size = New System.Drawing.Size(65, 17)
        Me.rbApprove.TabIndex = 7
        Me.rbApprove.TabStop = True
        Me.rbApprove.Text = "Approve"
        Me.rbApprove.UseVisualStyleBackColor = True
        '
        'cmdNot
        '
        Me.cmdNot.Location = New System.Drawing.Point(171, 3)
        Me.cmdNot.Name = "cmdNot"
        Me.cmdNot.Size = New System.Drawing.Size(113, 23)
        Me.cmdNot.TabIndex = 5
        Me.cmdNot.Text = "Not Approved"
        Me.cmdNot.UseVisualStyleBackColor = True
        '
        'cmdApprove
        '
        Me.cmdApprove.Location = New System.Drawing.Point(290, 3)
        Me.cmdApprove.Name = "cmdApprove"
        Me.cmdApprove.Size = New System.Drawing.Size(75, 23)
        Me.cmdApprove.TabIndex = 4
        Me.cmdApprove.Text = "Approve"
        Me.cmdApprove.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(371, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Get List"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'eDraw
        '
        Me.eDraw.Enabled = True
        Me.eDraw.Location = New System.Drawing.Point(12, 118)
        Me.eDraw.Name = "eDraw"
        Me.eDraw.OcxState = CType(resources.GetObject("eDraw.OcxState"), System.Windows.Forms.AxHost.State)
        Me.eDraw.Size = New System.Drawing.Size(976, 570)
        Me.eDraw.TabIndex = 4
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.RadioButton1)
        Me.FlowLayoutPanel2.Controls.Add(Me.RadioButton2)
        Me.FlowLayoutPanel2.Controls.Add(Me.Button2)
        Me.FlowLayoutPanel2.Controls.Add(Me.Button3)
        Me.FlowLayoutPanel2.Controls.Add(Me.Button4)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdRelease)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(12, 38)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(618, 34)
        Me.FlowLayoutPanel2.TabIndex = 3
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(91, 17)
        Me.RadioButton1.TabIndex = 6
        Me.RadioButton1.Text = "Not Approved"
        Me.RadioButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton1.UseVisualStyleBackColor = True
        Me.RadioButton1.Visible = False
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Checked = True
        Me.RadioButton2.Location = New System.Drawing.Point(100, 3)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(65, 17)
        Me.RadioButton2.TabIndex = 7
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Approve"
        Me.RadioButton2.UseVisualStyleBackColor = True
        Me.RadioButton2.Visible = False
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(171, 3)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(113, 23)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Not Approved"
        Me.Button2.UseVisualStyleBackColor = True
        Me.Button2.Visible = False
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(290, 3)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 4
        Me.Button3.Text = "Approve"
        Me.Button3.UseVisualStyleBackColor = True
        Me.Button3.Visible = False
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(371, 3)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "Single Print"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'cmdRelease
        '
        Me.cmdRelease.Location = New System.Drawing.Point(452, 3)
        Me.cmdRelease.Name = "cmdRelease"
        Me.cmdRelease.Size = New System.Drawing.Size(75, 23)
        Me.cmdRelease.TabIndex = 3
        Me.cmdRelease.Text = "Release"
        Me.cmdRelease.UseVisualStyleBackColor = True
        '
        'cbxPrinter
        '
        Me.cbxPrinter.FormattingEnabled = True
        Me.cbxPrinter.Location = New System.Drawing.Point(636, 64)
        Me.cbxPrinter.Name = "cbxPrinter"
        Me.cbxPrinter.Size = New System.Drawing.Size(219, 21)
        Me.cbxPrinter.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(138, 79)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Label1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(138, 92)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Label1"
        '
        'txtFFMID
        '
        Me.txtFFMID.Location = New System.Drawing.Point(653, 38)
        Me.txtFFMID.Name = "txtFFMID"
        Me.txtFFMID.Size = New System.Drawing.Size(100, 20)
        Me.txtFFMID.TabIndex = 8
        Me.txtFFMID.Visible = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1003, 24)
        Me.MenuStrip1.TabIndex = 9
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'Packing_SolutionDataSet
        '
        Me.Packing_SolutionDataSet.DataSetName = "Packing_SolutionDataSet"
        Me.Packing_SolutionDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Release_listBindingSource
        '
        Me.Release_listBindingSource.DataMember = "release_list"
        Me.Release_listBindingSource.DataSource = Me.Packing_SolutionDataSet
        '
        'Release_listTableAdapter
        '
        Me.Release_listTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.swmastTableAdapter = Me.SwmastTableAdapter
        Me.TableAdapterManager.UpdateOrder = ticket_printing.Packing_SolutionDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SwmastTableAdapter
        '
        Me.SwmastTableAdapter.ClearBeforeFill = True
        '
        'SwmastBindingSource
        '
        Me.SwmastBindingSource.DataMember = "swmast"
        Me.SwmastBindingSource.DataSource = Me.Packing_SolutionDataSet
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1003, 701)
        Me.Controls.Add(Me.txtFFMID)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbxPrinter)
        Me.Controls.Add(Me.eDraw)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ticket Printing"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        CType(Me.eDraw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.Packing_SolutionDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Release_listBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SwmastBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents DirectoryEntry1 As System.DirectoryServices.DirectoryEntry
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents rbNot As System.Windows.Forms.RadioButton
    Friend WithEvents rbApprove As System.Windows.Forms.RadioButton
    Friend WithEvents cmdNot As System.Windows.Forms.Button
    Friend WithEvents cmdApprove As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Private WithEvents AxEModelZoominControl1 As AxEModelViewZoomin.AxEModelZoominControl
    Friend WithEvents eDraw As AxEModelView.AxEModelViewControl
    Friend WithEvents Packing_SolutionDataSet As ticket_printing.Packing_SolutionDataSet
    Friend WithEvents Release_listBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Release_listTableAdapter As ticket_printing.Packing_SolutionDataSetTableAdapters.release_listTableAdapter
    Friend WithEvents TableAdapterManager As ticket_printing.Packing_SolutionDataSetTableAdapters.TableAdapterManager
    Friend WithEvents SwmastTableAdapter As ticket_printing.Packing_SolutionDataSetTableAdapters.swmastTableAdapter
    Friend WithEvents SwmastBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents cmdRelease As System.Windows.Forms.Button
    Friend WithEvents cbxPrinter As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtFFMID As System.Windows.Forms.TextBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
