﻿Imports System.Net.Mail
Imports EModelView
Imports System.Runtime.InteropServices
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Printing
Imports System.Data.SqlClient


Public Class frmMain
#Region "< Members >"
    Private _SMTPClient As String
    Dim sPrinter As String
    Dim m_Markup As EModelViewMarkup.EModelMarkupControl
    Dim nRow As Integer = 0
    Private sPath As String = ""
    Private dv As New DataView
    Private bRelease As Boolean = True
#End Region

    '  to add Third Party Drafters PDF files...
    '  I will have to search the filename string for the piece mark to be contained in the string to know which drawing to print out.
    '  Seems easy enough.

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click, Button4.Click

        'OpenFileDialog1.Multiselect = True
        OpenFileDialog1.Filter = "SLDDRW (*.slddrw)|*.slddrw"

        OpenFileDialog1.ShowDialog(Me)

        eDraw.Visible = True

        bRelease = True

        Dim obj() As Object = OpenFileDialog1.FileNames()
        'MsgBox(obj.Length)
        For Each f As String In OpenFileDialog1.FileNames
            eDraw.OpenDoc(f, False, False, False, "")
        Next
        bRelease = True
    End Sub
    '<DllImport("USER32.DLL", CharSet:=System.Runtime.InteropServices.CharSet.Auto)> _
    'Public Shared Function WaitForInputIdle(ByVal handle As System.Diagnostics.Process, ByVal milliseconds As Integer) As Integer
    'End Function
    Private Sub cmdApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim m_Markup As EModelViewMarkup.EModelMarkupControl
        m_Markup = eDraw.CoCreateInstance("EModelViewMarkup.EmodelMarkupControl")
        m_Markup.AddStamp("C:\Program Files\Common Files\eDrawings2008\lang\english\approved.png", 0.15, 0.2, 0.03, 0.01)
        'eDraw.Save(eDraw.FileName, False, "")
        'MsgBox("saving")

        'eDraw.CloseActiveDoc("")
    End Sub

    Private Sub cmdNot_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim m_Markup As EModelViewMarkup.EModelMarkupControl
        m_Markup = eDraw.CoCreateInstance("EModelViewMarkup.EmodelMarkupControl")
        m_Markup.AddStamp("C:\Program Files\Common Files\eDrawings2008\lang\english\notapproved.png", 0.2, 0.2, 0.035, 0.015)
        eDraw.Save(eDraw.FileName, False, "")
        MsgBox("saving")
        send_mail(eDraw.FileName, "ticket needs revised")
        'eDraw.CloseActiveDoc("")
    End Sub
    Private Sub send_mail(ByVal sBody As String, ByVal sMsg As String)
        Dim myMessage As New MailMessage
        Dim sFrom As String = System.Environment.UserName
        sFrom &= "@readingrock.com"

        myMessage.IsBodyHtml = True 'or MailFormat.Text 
        myMessage.Priority = MailPriority.High ', High, or Low 
        myMessage.From = New MailAddress(sFrom, "Mold Master") 'From Address
        myMessage.To.Add(sFrom) ' jmikel@cinci.rr.com" 'Send a Blind Carbon-Copy 
        myMessage.Subject = sMsg
        ' generate string or report

        myMessage.Body = sBody

        Dim smtp As New SmtpClient(_SMTPClient)
        'smtp.Send(myMessage)

        myMessage = Nothing

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cmdApprove.Visible = False
        cmdNot.Visible = False
        For Each printer As String In System.Drawing.Printing.PrinterSettings.InstalledPrinters
            cbxPrinter.Items.Add(printer)
        Next printer
        Try
            m_Markup = eDraw.CoCreateInstance("EModelViewMarkup.EmodelMarkupControl")
        Catch ex As Exception
            MsgBox(ex.Message.ToString(), MsgBoxStyle.Exclamation, "Error 1")
        End Try
        Try
            Using ta As New Packing_SolutionDataSetTableAdapters.QueriesTableAdapter
                _SMTPClient = ta.spGetSMTPServerName(1).ToString.Trim
            End Using
        Catch ex As SqlException
            Using taError As New Packing_SolutionDataSetTableAdapters.QueriesTableAdapter
                taError.insAppErrors("Default", Me.GetType.Name, ex.Message.ToString, System.Environment.UserName)
            End Using
        End Try
    End Sub

    '#If Win32 Then
    '    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    '    Const WM_SYSCOMMAND = &H112&
    '    Const SC_SCREENSAVE = &HF140&
    '#Else
    '    Private Declare Function SendMessage Lib "User" (ByVal hWnd As Integer, ByVal wMsg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As Long
    '    Const WM_SYSCOMMAND = &H112
    '    Const SC_SCREENSAVE = &HF140&
    '#End If

    '    Private Sub Command1_Click()
    '        Dim result As Long
    '        result = SendMessage(Me.Handle, WM_SYSCOMMAND, SC_SCREENSAVE, 0&)
    '    End Sub



    Private Sub cmdRelease_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRelease.Click
        Label1.Text = ""
        Release_listTableAdapter.Fill(Packing_SolutionDataSet.release_list, Now.Date, 5)
        dv.Table = Packing_SolutionDataSet.release_list
        dv.Sort = "xf desc, exp desc, fsono, fdrawno, finumber"
        If (txtFFMID.Text.Trim.Length > 0) Then
            If IsNumeric(txtFFMID.Text) Then
                dv.RowFilter = "ffmid = '" & txtFFMID.Text & "'"
            End If
        Else
            dv.RowFilter = Nothing
        End If
        get_path(0)

    End Sub
    Public Sub print_placeholder(ByVal sPieceMark As String, ByVal sFfmid As String)
        Dim rpt As New rptPlaceHolder
        rpt.FileName = "\\rrs0009\public\RRPRS\Packing Solutions\crystal reports\rptPlaceHolder.rpt"
        rpt.SetParameterValue("piece_mark", sPieceMark)
        rpt.SetParameterValue("ffmid", sFfmid)
        'Dim outputfile As String
        'outputfile = "\\rrs0005\m2msql50\caginc\uploads\packinglist.xls"
        'rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, outputfile)
        rpt.PrintOptions.PrinterName = sPrinter
        rpt.PrintToPrinter(1, False, 0, 0)
        rpt.Dispose()
        nRow += 1
        get_path(nRow)
    End Sub

    Private Sub cbxPrinter_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxPrinter.SelectionChangeCommitted
        sPrinter = cbxPrinter.SelectedItem
    End Sub
    Private Sub get_path(ByVal nRow As Integer)
        Dim sFfmid As String = ""
        Dim sPieceMark As String = ""
        Dim dvSW As New DataView
        dvSW.Table = Packing_SolutionDataSet.swmast
        Dim nSWRow As Integer = 0
        Dim sRootPath As String = "\\rrs0009\Operations\RockCast\"

        OpenFileDialog1.Filter = "SLDDRW (*.slddrw)|*.slddrw"
        If dv.Count = 0 Then
            MsgBox("There are no JO's on Shift 5 for today.", MsgBoxStyle.Information)
            Return
        End If

        If sFfmid <> dv(nRow)("ffmid") Then
            sFfmid = dv(nRow)("ffmid")
            SwmastTableAdapter.Fill(Packing_SolutionDataSet.swmast, sFfmid)
            dvSW.Sort = "piece_mark"
            If dv(nRow)("swrel") > 0 Then
                dvSW.RowFilter = "release = " & dv(nRow)("swrel")
            End If
            nSWRow = dvSW.Find(dv(nRow)("piece_mark"))
        End If
        'SwmastBindingSource.CurrencyManager.Position = SwmastBindingSource.Find("piece_mark", dv(nRow)("piece_mark"))
        If nSWRow <> -1 Then
            If dvSW(nSWRow)("third_party").ToString.Trim.Length = 0 Then
                ' Should have solid works drawing.

                sPath = sRootPath & dvSW(nSWRow)("extract_path").ToString() & dv(nRow)("piece_mark") & ".SLDDRW"
                If File.Exists(sPath) Then
                    eDraw.OpenDoc(sPath, False, False, False, "")
                Else
                    sPath = sRootPath & dvSW(nSWRow)("extract_path").ToString() & dv(nRow)("piece_mark") & "_.SLDDRW"
                    If File.Exists(sPath) Then
                        eDraw.OpenDoc(sPath, False, False, False, "")
                    Else
                        ' Release possibly not used or drawings moved to new folder for some reason.
                        ' remove the release filter and add piece_mark filter. The try again for each row.
                        dvSW.RowFilter = "piece_mark = '" & dv(nRow)("piece_mark") & "'"
                        Dim bFound As Boolean = False
                        For Each dr As DataRowView In dvSW
                            sPath = sRootPath & dr("extract_path").ToString() & dv(nRow)("piece_mark") & ".SLDDRW"
                            If File.Exists(sPath) Then
                                eDraw.OpenDoc(sPath, False, False, False, "")
                                bFound = True
                                Exit For
                            Else
                                sPath = sRootPath & dr("extract_path").ToString() & dv(nRow)("piece_mark") & "_.SLDDRW"
                                If File.Exists(sPath) Then
                                    eDraw.OpenDoc(sPath, False, False, False, "")
                                    bFound = True
                                    Exit For
                                End If
                            End If
                        Next
                        If Not bFound Then
                            print_placeholder(dv(nRow)("piece_mark"), dv(nRow)("ffmid").ToString.Trim)
                        End If
                    End If
                End If
                Label2.Text = sPath
            Else
                ' Should have PDF file from third party drafters.
                'Projects\3235 - Penn Wells\Rockcast\Drawings\Shop Tickets
                Dim sDirectory As String = sRootPath & dvSW(nSWRow)("extract_path").ToString()
                Dim sPM As String = dvSW(nSWRow)("third_party").ToString.ToUpper.Trim()

                If Directory.Exists(sDirectory) Then
                    ' Should have PDF file from third party drafters.
                    'Projects\3235 - Penn Wells\Rockcast\Drawings\Shop Tickets
                    Dim dt As New Packing_SolutionDataSet.swmastDataTable
                    Dim bFound As Boolean = False
                    Dim sFileToPrint As String = ""
                    If Directory.Exists(sDirectory) Then
                        ' get all PDF files that start with alpha from folder.
                        ' look for exact match.  If not found, then look for files that the numeric will be in the 'range'
                        ' find the string
                        Dim nFileInt As Integer = 0
                        Dim sFileInt As String = ""
                        Dim sAlpha As String = ""
                        For Each c As Char In sPM
                            If IsNumeric(c) Then
                                sFileInt &= c
                            Else
                                sAlpha &= c
                            End If
                        Next
                        nFileInt = Integer.Parse(sFileInt)

                        ' exact match is a file exists not an array of one file... duh
                        ' 05/04/2012 JRM - add four lines below and remarked out the array for exact match.
                        If IO.File.Exists(sDirectory + "\" + sPM + ".PDF") Then
                            bFound = True
                            sFileToPrint = sDirectory + "\" + sPM + ".PDF"
                        End If
                        Dim arrFiles As String() = Directory.GetFiles(sDirectory, "*.PDF")
                        '' Exact match search
                        'For Each f As String In arrFiles
                        '    Dim fi As New FileInfo(f)
                        '    If sPM & ".PDF" = fi.Name.ToUpper Then
                        '        bFound = True
                        '        MsgBox("found")
                        '        sFileToPrint = f
                        '    End If
                        'Next
                        If Not bFound Then
                            arrFiles = Directory.GetFiles(sDirectory, sAlpha & "*to*.PDF")
                            For Each f As String In arrFiles
                                Dim fi As New FileInfo(f)
                                Dim nBeginRange As Integer = 0
                                Dim sBeginRange As String = ""
                                Dim nEndRange As Integer = 0
                                Dim sEndRange As String = ""
                                Dim arrFile As String() = fi.Name.Split(" ")
                                For Each c As String In arrFile(0).Trim()
                                    If IsNumeric(c) Then
                                        sBeginRange &= c
                                    End If
                                Next
                                nBeginRange = Integer.Parse(sBeginRange)
                                For Each c As String In arrFile(2).Replace(".pdf", "").Trim()
                                    If IsNumeric(c) Then
                                        sEndRange &= c
                                    End If
                                Next
                                nEndRange = Integer.Parse(sEndRange)
                                If nBeginRange <= nFileInt And nFileInt <= nEndRange Then
                                    bFound = True
                                    sFileToPrint = f
                                End If
                            Next
                        End If
                        If Not bFound Then
                            arrFiles = Directory.GetFiles(sDirectory, sAlpha & "*-*.PDF")
                            For Each f As String In arrFiles
                                Dim fi As New FileInfo(f)
                                Dim nBeginRange As Integer = 0
                                Dim sBeginRange As String = ""
                                Dim nEndRange As Integer = 0
                                Dim sEndRange As String = ""
                                Dim arrFile As String() = fi.Name.Split(" ")
                                For Each c As String In arrFile(0).Trim()
                                    If IsNumeric(c) Then
                                        sBeginRange &= c
                                    End If
                                Next
                                nBeginRange = Integer.Parse(sBeginRange)
                                For Each c As String In arrFile(2).Trim()
                                    If IsNumeric(c) Then
                                        sEndRange &= c
                                    End If
                                Next
                                nEndRange = Integer.Parse(sEndRange)
                                If nBeginRange <= nFileInt And nFileInt <= nEndRange Then
                                    bFound = True
                                    sFileToPrint = f
                                End If
                            Next
                        End If
                        If Not bFound Then
                            arrFiles = Directory.GetFiles(sDirectory, sAlpha & "*.PDF")
                            For Each f As String In arrFiles
                                Dim fi As New FileInfo(f)
                                Dim arrFile As String() = fi.Name.Split(" ")
                                If Not fi.Name.Contains("-") And Not fi.Name.Contains("to") And arrFile.Length > 1 Then
                                    For i As Integer = 0 To arrFile.Length - 1
                                        If sPM = arrFile(i).ToUpper.Replace(".PDF", "") Then
                                            bFound = True
                                            sFileToPrint = f
                                        End If
                                    Next
                                End If
                            Next
                        End If
                    End If

                    If bFound Then
                        ' print the PDF file.
                        PrintPDF(sFileToPrint)
                    Else
                        ' need to print place holder.
                        print_placeholder(dv(nRow)("piece_mark"), dv(nRow)("ffmid").ToString.Trim)
                    End If
                Else
                    MsgBox(sDirectory & " does not exist." & vbCrLf & "Will not find drawings for " & sFfmid & " Project ID." & vbCrLf & "Easiest thing to do would be to create the directory and move the PDF files into it.")
                End If

            End If
        Else
            ' STANDARD PART search.
            If dv(nRow)("piece_mark").ToString.Trim.Length > 9 Then
                If dv(nRow)("piece_mark").ToString.IndexOf("-", 9) > 0 Then
                    sPieceMark = dv(nRow)("piece_mark").ToString.Substring(0, dv(nRow)("piece_mark").ToString.IndexOf("-", 9))
                Else
                    sPieceMark = dv(nRow)("piece_mark")
                End If
            Else
                sPieceMark = dv(nRow)("piece_mark")
            End If
            sPath = "\\rrs0009\Operations\RockCast\STANDARD PARTS\Shop Tickets\" & sPieceMark & ".SLDDRW"
            If File.Exists(sPath) Then
                eDraw.OpenDoc(sPath, False, False, False, "")
            Else
                sPath = "\\rrs0009\Operations\RockCast\STANDARD PARTS\Shop Tickets\" & sPieceMark & "_.SLDDRW"
                If File.Exists(sPath) Then
                    eDraw.OpenDoc(sPath, False, False, False, "")
                Else
                    ' Release possibly not used or drawings moved to new folder for some reason.
                    ' remove the release filter and add piece_mark filter. The try again for each row.
                    dvSW.RowFilter = "piece_mark = '" & dv(nRow)("piece_mark") & "'"
                    Dim bFound As Boolean = False
                    For Each dr As DataRowView In dvSW
                        sPath = sRootPath & dr("extract_path").ToString() & dv(nRow)("piece_mark") & ".SLDDRW"
                        If File.Exists(sPath) Then
                            eDraw.OpenDoc(sPath, False, False, False, "")
                            bFound = True
                            Exit For
                        Else
                            sPath = sRootPath & dr("extract_path").ToString() & dv(nRow)("piece_mark") & "_.SLDDRW"
                            If File.Exists(sPath) Then
                                eDraw.OpenDoc(sPath, False, False, False, "")
                                bFound = True
                                Exit For
                            End If
                        End If
                    Next
                    If Not bFound Then
                        print_placeholder(dv(nRow)("piece_mark"), dv(nRow)("ffmid").ToString.Trim)
                    End If
                End If
            End If
        End If

    End Sub

    Private Sub eDraw_OnFinishedPrintingDocument(ByVal sender As System.Object, ByVal e As AxEModelView._IEModelViewControlEvents_OnFinishedPrintingDocumentEvent) Handles eDraw.OnFinishedPrintingDocument
        If bRelease Then
            eDraw.CloseActiveDoc("")
            nRow += 1
            If nRow <= dv.Count Then
                get_path(nRow)
            End If
        End If
    End Sub

    Private Sub eDraw_OnFinishedLoadingDocument(ByVal sender As System.Object, ByVal e As AxEModelView._IEModelViewControlEvents_OnFinishedLoadingDocumentEvent) Handles eDraw.OnFinishedLoadingDocument
        If bRelease Then
            m_Markup.AddStamp("C:\Program Files\Common Files\eDrawings2008\lang\english\approved.png", 0.25, 0.2, 0.03, 0.01)
            'eDraw.SetPageSetupOptions(EMVPrintOrientation.eLandscape, 1, 0, 0, 1, 7, "BizHub 200", 0, 0, 0, 0)
            eDraw.Print3(False, "edrawing", False, False, False, EMVPrintType.eScaleToFit, 0, 0, 0)
        End If
    End Sub

    Private Sub AboutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutToolStripMenuItem.Click
        Dim obj As New AboutBox1
        obj.ShowDialog(Me)
        obj.Dispose()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        End
    End Sub
    Private Sub PrintPDF(ByVal f As String)
        Try
            Dim psi As New ProcessStartInfo
            psi.UseShellExecute = True
            psi.Verb = "print"
            psi.WindowStyle = ProcessWindowStyle.Hidden
            'psi.Arguments = PrintDialog1.PrinterSettings.PrinterName.ToString()
            psi.FileName = f ' Here specify a document to be printed

            Process.Start(psi)
            done_printingPDF()

            'Dim d As New PrintDocument
            'AddHandler d.EndPrint, AddressOf done_printingPDF
            'd.DocumentName = f
            'd.PrinterSettings.PrinterName = sPrinter
            'd.Print()
        Catch ex As Exception
            MsgBox(ex.Message.ToString())
        End Try
    End Sub
    Private Sub done_printingPDF()
        nRow += 1
        If nRow <= dv.Count - 1 Then
            get_path(nRow)
        End If
    End Sub
End Class
